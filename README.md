Hello World!

# link 
[gitlab] (http://gitlab.com)


# Header 1
## Header 2
### Header 3

# Learning Unit 9

## Relationships of Quantopian and Zipline?
Zipline is a trading algorithm that is used, and continuously updated by Quantopian. It allows for life-trading, as well as back-testing of trades and trading strategies.

## What does set_slippage function do?
It enables price manipulations by our trades on historical data when back testing. So that the trades that we execute drive the stock price up or down, which would be the case if we would have bought large amounts of the market of one stock. 
Also it limits the amount of stocks to be traded, to the usual market volume cap or the maximum volume available.
Slippage can have various additional limits or arguments, such as the magnitude to which your orders will affect prices in basis points, or you might put a volume limit. 

When using Fixed Slippage, the orders you place do not affect the price of the stocks

## What does the handle_data function do?
It calls for each event the context (every minute), which is to be set and includes various variables. The context has to be defined in the initialize() function Further it calls data which is typically the data concerning the stock, e.g. OHLC (Open, High, Low, Close)

But you may also use trading start, which only updates & executes the trade once a day, at the beginning of the trading day. 

## Is the _test_arg function part of the Zipline framework?
No, it is not 

## Other variables

### Context 
Is a shell where you store the variables you will recall in each iteration
### Data
Is the data you want to acces OHLC (open, high, low, close) of the stock you are observing/using in your trading bot


## Other functions
### Initialize()
Called always in the beginning of a back testing task
You can call what ever bookkeeping you would like to test (context) 
Calles the context (determined set of variables)
What does “pass” mean?
### Ingest 
Ingesting takes a data bundle (set default by Quantipian)
### Order()
Takes two arguments (1. Security, 2. Quantity to buy) 
Those are then bought in each iteration
### Record()
Records the progress / trades or actions undertaken by the bot in each iteration to document it


## Moin, this is just a test.

## Test this now
Just to see if this works: So the idea is, that i can change things here, then I see them on GitKraken to confirm them, if I press push I transfer them onto Gitlab and have them online then.
Then if I do changes I stage them, then I add a comment and a summary of the changes, from there I push them to get them online.
If I press pull, do i get what I changed online to transfer offline?
# Yes this works perfectly. If I pull stuff, i update the offline document with the content i added online!! -> Amazing




# New code of the Quantopian Tradingbot

"""
This is a template algorithm on Quantopian for you to adapt and fill in.
"""
import quantopian.algorithm as algo
from quantopian.pipeline import Pipeline
from quantopian.pipeline.data.builtin import USEquityPricing
from quantopian.pipeline.filters import QTradableStocksUS


def initialize(context):
    # we select the securities, that we want
    context.security = symbol ('TSLA')
    
    #schdule function, will run the orders every day at market opening
    schedule_function(func = action_function,
                       date_rule = date_rules.every_day(),
                       time_rule = time_rules.market_open(hours=0,                         minutes=1))
    
def action_function(context, data): # does 3 things: gets current price, makes an order, within a major exchange
    #Prices for the last 5 days of our security
    price_history = data.history(assets = context.security,
                                  fields = 'price', bar_count=10,
                                 frequency = '1d')
    
    average_price = price_history.mean()
    
    current_price = data.current(assets=context.security, fields = 'price')
    
    
    if data.can_trade(context.security):
        #This will by me 1000$ worth in shares of TSLA
        if current_price > average_price *1.03:
            order_target_percent(context.security, 1) # orders the amount of securities that we want to purchase also short selling is possible
        else:
            #This will set my positions to zero
            order_target_percent(context.security, 0)
