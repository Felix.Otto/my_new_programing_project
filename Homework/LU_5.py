#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 10:39:05 2018

@author: FelixFischer
"""



if True:
    print('hello world')
    
hello world

if False:
    print ('not true')
    

a = True

if a: 
    print('a is True')
    
a is True

if a:
    print('hello',a)
    
hello True

b = False

if b:
    print('hello', b)
    

if 1:
    print ('hello', 1)
    
hello 1

if 0: 
    print ('hello', 0)
    

if 2:
    print ('hello', 2)
    
    
if '':
    print ('hello', '')
    

if 'world':
    print('hello', 'wrold')
    
    
    
bool (1)
Out[16]: True

bool ('moin')
Out[17]: True

bool ('World')
Out[18]: True

bool (0)
Out[19]: False

bool (False)
Out[20]: False


f True: 
    print ('hello world')
    
hello world

def return_true ():
    return True

if return_true():
    print ('hello world')
    
hello world

val = return_true ()

if val:
    print ('hello world')
    
hello world


a = True

b = False

if a or a:
    print ('at least one thing is true')
    
at least one thing is true

if a or b:
    print ('at least one thing is true')
    
at least one thing is true

if b or b:
    print ('at least one thing is true')
    

if b or b or b or b or b or a:
    print ('at least one thing is true')
    
at least one thing is true


*EX IF, ELIF, ELSE

if ticker_value == 'APPL':
    print ('buy two shares in APPL')
elif ticker_value == 'GOOG':
    print ('buy two shares in GOOG')
else: 
    print ('dont buy anything')
    

*Calling Lists & Dicts



 a_list = [1,2,3]

for num in a_list:
    print(num)
    
1
2
3

a_dict = {'ALLP': 1, 'GOOG': 2, 'AZO': 3}

for key in a_dict:
    print ('key', key, 'value', a_dict[key]) 
    
key ALLP value 1
key GOOG value 2
key AZO value 3

for key in a_dict:
    print (a_dict[key])
    
1
2
3

for key in a_dict:
    print ('key', key)
    
key ALLP
key GOOG
key AZO

for key in a_dict:
    print (key, ':', a_dict[key])
    
ALLP : 1
GOOG : 2
AZO : 3




*** EX 1 *** 



bool (1)
Out[43]: True

bool (1.1)
Out[44]: True

bool (0)
Out[45]: False

bool (0.0001)
Out[46]: True

bool ('')
Out[47]: False

bool (None)
Out[48]: False



*** EX 2 ***




ticker_symbol = 'APPL'

if ticker_symbol == 'APPL':
    print ('APPL')
else:
    print ('False')
    
APPL

ticker_symbol = 'GOOG'

if ticker_symbol == 'APPL':
    print ('APPL')
else:
    print ('False')
    
False




*** EX 3 ***




gender = 'male'

if gender == 'male':
    print ('you hate pink')
elif gender == 'female':
    print ('you like pink')
else:
    print ('no gender information')




*** EX 4 ***




 def gender (gender):
    if gender == 'male':
        print ('you hate pink')
    if gender == 'female':
        print ('you like pink')
    print ('this line should not be printed')
    

gender ('male')
you hate pink
this line should not be printed

** Fix:

def gender (gender):
    if gender == 'male':
        print ('you hate pink')
    if gender == 'female':
        print ('you like pink')
        

gender ('female')
you like pink




*** EX 5 ****




a = 53
b = 49
 
def parents_age (age_dad , age_mom):
    if a == b:
        c = a + b
        print ('The sum of both is =',c)
    else:
        d = a - b
        print ('The difference of the ages is', d)
        



parents_age (a, b)
The difference of the ages is 14

a = 50

b = 50 

parents_age (a, b)
The sum of both is = 100




*** EX 6 ***




a_list = [1,2,3]

a_dict = {'ALLP': 1, 'GOOG': 2, 'AZO': 3}



len (a_list)
Out[85]: 3

len (a_dict)
Out[86]: 3



def comp_list_dict (list, dict):
    if len (a_list) == len (a_dict):
        print ('That is awesome')
    else:
        print ('That is just sad')

comp_list_dict (a_list, a_dict)
That is awesome

a_list.append (4)

comp_list_dict (a_list, a_dict)
That is just sad





*** EX 7 ***




a_dict = {'ALLP': 100, 'GOOG': 80, 'AZO': 60}

 for key in a_dict:
    print (key)
    
ALLP
GOOG
AZO

for key in a_dict:
    print (a_dict[key])
    
100
80
60

for key in a_dict:
    print('Todays value of', key, 'is', a_dict[key])
    
Todays value of ALLP is 100
Todays value of GOOG is 80
Todays value of AZO is 60





*** EX 8 ***



a_tuple = (10, 20, 30)

a_list = [40, 50, 60]

a_dict = {'APPL': 100, 'GOOG': 90, 'FB': 80}


for num in a_tuple:
    print (num)
    

for num in a_list:
    print (num)


for key in a_dict:
    print (a_dict[key])

**** OR

for key in a_dict:
    print (key)





*** EX 9 ***

a_list = [1, 2, 3]

my_new_list = [i * 5 for i in a_list]

print(my_new_list)
[5, 10, 15]
    
    
*** EX 10 ***

for key in a_dict:
    a_dict[key] *= 2
    print (key, a_dict[key])
    
APPL 200.0
GOOG 180.0
FB 160.0    





*** EX 11 ***



a = 3

a_list = [1,2]

if a > len(a_list):
    print ('Number is greater than items')
else:
    print ('List is greater or equal')
    
Number is greater than items


a_list.append (3)

if a > len(a_list):
    print ('Number is greater than items')
else:
    print ('List is greater or equal')
    
List is greater or equal

