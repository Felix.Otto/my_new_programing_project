#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 19:25:15 2018

@author: FelixFischer
"""

def calculation (n):
    ret_val = n / 2
    return ret_val

a = 20

b = calculation (a)

print('a divided by 2 is', b)
a divided by 2 is 10.0



** EXERCISE 1


def powers (a, b):
    ret_val = a ** b
    return ret_val


a = 2

b = 3

p = powers (a, b)

print (p)
8


** EXERCISE 2

 def is_equal (a, b):
    ret_val = a == b
    return ret_val


a = 10 

b = 11

is_equal (a, b)
Out[90]: False

x = 30

y = 30

is_equal (30, 30)
Out[93]: True

c = is_equal

c = is_equal (x, y)

print (c)
True

check (y, x)
Out[97]: True



** EXERCISE 3


def hypothenuse (a, b):
    ret_val = a ** 2 + b ** 2
    return ret_val


a = 2

b = 2

hypothenuse (a, b)
Out[101]: 8

h = hypothenuse (a, b)

print (h)
8

x = 10 

y = 10

hypothenuse (y, x)
Out[106]: 200


** EXERCISE 4

def bit_lite (b):
    ret_val = b * 51.31
    return ret_val

def bit_eur (a):
    ret_val = a * 6971.54
    return ret_val

def bit_ethe (c):
    ret_val = c * 10.22
    return ret_val


** EXERCISE 5

// A function without the return statement, does the calculation it was designed to do, given the defined input, however since the return statement is not defined, it will not deliver any output. This means the user cannot read the result. 


** EXERCISE 6

//For the print function you first need to assign the function to a variable ( squ = squared) 

A Return only gives you the return that  the machine (the calculation) does for the variables you enter. 
With print it allows you to give the operator (e.g. the one who runs the program) information about what just happened. 


** EXERCISE 7

// In: the input delivered from the use. 
If nothing is stated, then the REPL just mirrors the input

Out: the REPL gives an output of what has been previously defined 
