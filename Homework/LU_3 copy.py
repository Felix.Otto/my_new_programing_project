# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


** EXERCISE 1

// Scope defines to what extend / in which context my defined variables are able
to be accessed. 

// Local means that the definition given can only be used and
accessed in the local area (e.g. the function). Here the variable is defined
within the body/machinery of the function

// Global means the defined variable can be used and accessed
in all functions (e.g. defined for all) at a global scope it does not matter if the 
variable is defined before or after the function. However, it has to be defined before the function
is called.

** EXERCISE 2

first_var = 10			// Global

del hello_world():
	who = ‘world’		// Local
	print(‘hello’, who)	

second_var = 20 		// Global


def hello_other_world():
	who_again = ‘alien_world’	// Local
	print(who_again)		



** EXERVISE 3

print (‘hello, first_var)

first_var = 10


// first_var is only defined after the 
print function is being called.
There will be no output for first_var

Name Error



** EXERCISE 4

a = 10 

def multiply ():
    ret_val = a * 2
    return ret_val


multiply ()
Out[27]: 20

c = multiply ()

print (a, c)
10 20



** EXERCISE 5 

power = 10
print (power)
10

def generate_power (number):
    power = 2
    def nth_power ():
        return number ** power
    powered = nth_power()
    return powered


print (_power_)

a = generate_power (2)

print (a)
4



** EXERCISE 6 


power = 10
print (power)
10

def generate_power (number):
    power = 2
    def nth_power ():
        return number ** power
    powered = nth_power()
    return powered


print (power)

a = generate_power (2)

print (a)
4

//If I print power, it will give me 10 since this is in a global scope

If I print generate_power, where the power is 2 and the number to 
be powered is defined as n (in this case: 2). Thus, the output will be 
the calculation defined within the function generate_power. Using
2^10 for the calculation.


** EXERCISE 7

_what = ‘coins’
_for = ‘chocolates’

def transaction (_what, _for):
	_what = ‘oranges’
	_for = ‘bananas’
	print(_what, _for)

tranaction (_what, _for)
print (_what, _for)

// No, 

The term ‘transaction (_what, _for)’ will print:
oranges bananas

The second term ‘print (_what, _for)’ will print:
coins chocolates

This is because in the first we are referring to the function specifically,
the second calls the globally defined values for _what & _for, e.g. coins
and chocolates respectively.



** EXERCISE 9 


def name_funct (first_name = 'felix', last_name = 'otto'):
    print (first_name, last_name)
    

name_funct ()
felix otto

name_funct (first_name = ‘bart’,):
bart otto

name_funct (first_name = ‘mat’, last_name = ‘fischer’):
mat fischer


** EXERCISE 10

name () // will print Sam Hopkins
name (first_name = frist_name, last_name = last_name) // will print Ricardo Pereira
name (first_name = ‘Jose’, last_name = ‘Maria’) // will print Jose Maria

// the prints will be due to the default setting of Sam Hopkins in the function




** EXERCISE 11


def greeting (message = ‘Good morning’, name = ‘Ricardo’):
	print (message, name)

greeting ()
Good morning Ricardo



greeting (message = ‘You know nothing’, name = ‘John Snow’)
You know nothing John Snow

